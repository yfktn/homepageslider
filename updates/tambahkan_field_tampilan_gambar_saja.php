<?php namespace Yfktn\HomepageSlider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Tambahkan fasilitas berupa opsi untuk menampilkan gambar saja pada carousel
 * @package Yfktn\HomepageSlider\Updates
 */
class TambahkanFieldTampilanGambarSaja extends Migration
{
    public function up()
    {
        Schema::table('yfktn_homepageslider_utama', function ($table) {
            $table->integer('gambar_saja')->unsigned()->nullable();
        });
    }

    public function down()
    {
        // nothing to do ... just leave it!
    }
}
