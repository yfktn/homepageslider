<?php namespace Yfktn\HomepageSlider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnHomepagesliderUtama extends Migration
{
    public function up()
    {
        Schema::create('yfktn_homepageslider_utama', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('judul');
            $table->string('deskripsi01');
            $table->string('deskripsi02')->nullable();
            $table->string('deskripsi03')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('link')->default('#');
            $table->string('slug')->index()->nullable();
            $table->integer('sort_order')->index()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_homepageslider_utama');
    }
}