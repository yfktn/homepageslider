<?php namespace Yfktn\HomepageSlider\Models;

use Model;

/**
 * Model
 */
class HomepageSlider extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_homepageslider_utama';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'judul' => 'required',
        'slug'    => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:yfktn_homepageslider_utama'],
        'deskripsi01' => 'required'
    ];
    
    /**
     * Ini untuk file
     * @var type 
     */
    public $attachOne = [
        'gambarHeader' => [ 'System\Models\File', 'public' => true ]
    ];
    
}
