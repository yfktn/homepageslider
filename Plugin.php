<?php

namespace Yfktn\HomepageSlider;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\Yfktn\HomepageSlider\Components\HomepageSlider' => 'homepageSlider',
            '\Yfktn\HomepageSlider\Components\B3HomepageSlider' => 'b3HomepageSlider'
        ];
    }

    public function registerSettings()
    {
    }
}
