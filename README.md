## Homepage Slider

Ini merupakan plugin sederhana untuk menambahkan slider, dengan menu dan record yang bisa dirubah tanpa harus selalu membuat hal yang baru lagi, kecuali merubah template saja.

Terdapat dua buah component sementara ini:

- komponen untuk OWL Slider
- komponen untuk Bootstrap 3
  
Perhatian bahwa css maupun asset lain yang dibutuhkan tidak disediakan, dan diserahkan kepada masing-masing developer untuk menambahkannya di themes.

### Cara penggunaan

Sekarang sudah bisa di install lewat composer dengan Cara

```
$ composer require yfktn/homepageslider
```

Lakukan cloning repository ke folder plugins di project OctoberCMS yang digunakan.

```
$ cd octobercms-prj/plugins
$ mkdir yfktn
$ cd yfktn
$ git clone https://gitlab.com/yfktn/homepageslider homepageslider
```

Kemudian lakukan penambahan record pada menu Homepage Slider dan tambahkan komponen melalui CMS Menu di OctoberCMS.

Lisensi: MIT
