<?php

namespace Yfktn\HomepageSlider\Components;

use Yfktn\HomepageSlider\Models\HomepageSlider as HomepageSliderModel;

/**
 * Menampilkan homepage slider menggunakan Bootstrap 3
 *
 * @author toni
 */
class B3HomepageSlider extends \Cms\Classes\ComponentBase
{
    //put your code here
    public function componentDetails()
    {
        return [
            'name'        => 'Bootstrap 3  Homepage Slider',
            'description' => 'Menampilkan slider di homepage pake Bootstrap 3'
        ];
    }

    public function defineProperties()
    {
        return [
            'jumlahItem' => [
                'title' => 'Item Tampilan',
                'description' => 'Jumlah item ditampilkan',
                'type' => 'string',
                'default' => 5
            ],
        ];
    }

    public function siapkanVariable()
    {
        $this->page['jumlahItem'] = $this->property('jumlahItem', 5);
    }

    public function dapatkanData()
    {
        $o = HomepageSliderModel::with('gambarHeader')
            ->limit((int) $this->page['jumlahItem'])
            ->get();
        return $o;
    }

    public function onRun()
    {
        $this->siapkanVariable();
        $this->page['posts'] = $this->dapatkanData();
    }
}
