<?php namespace Yfktn\HomepageSlider\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class HomepageCarousel extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'atur-hompeage-carousel' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Yfktn.HomepageSlider', 'main-menu-homepage-carousel');
    }
}
